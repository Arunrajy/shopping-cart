import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from "./components/home/home.component";
import { ProductListingComponent } from "./components/product-listing/product-listing.component";
import { ProductListingSidebarComponent } from "./components/product-listing-sidebar/product-listing-sidebar.component";
import { ProductDetailComponent } from "./components/product-detail/product-detail.component";
import { CartComponent } from "./components/cart/cart.component";
import { CheckoutComponent } from "./components/checkout/checkout.component";
import { HelpComponent } from "./components/help/help.component";
import { TrackOrderComponent } from "./components/track-order/track-order.component";
import { ConfirmComponent } from "./components/confirm/confirm.component";
import {ContactComponent } from "./components/contact/contact.component";

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent},
  { path: 'product-list', component: ProductListingComponent},
  { path: 'product-list-sidebar', component: ProductListingSidebarComponent},
  { path: 'product-list/:id', component: ProductDetailComponent},
  { path: 'cart', component: CartComponent},
  { path: 'checkout', component: CheckoutComponent},
  { path: 'help', component: HelpComponent},
  { path: 'track-order', component: TrackOrderComponent},
  { path: 'confirm', component: ConfirmComponent},
  { path: 'contact', component: ContactComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
