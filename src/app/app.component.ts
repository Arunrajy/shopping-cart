import { Component, ViewChild, HostListener, ElementRef,Inject, PLATFORM_ID, ViewEncapsulation } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { Router, NavigationEnd } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {
  title = 'shopping-cart';
  public isMenuCollapsed = true;
  public isSearchCollapsed = true;
  initialload:any;
  @ViewChild("mainFooter" , {static : false}) footer :  ElementRef;
  mainMarginBottom:any = 0;
  isShow: boolean;
  topPosToStartShowing:any = 100;

  constructor(@Inject(PLATFORM_ID) private platformId: Object, private router: Router){}
  @HostListener('window:scroll', ['$event'])

  ngOnInit(){
    this.initialload = false;
    
    // scroll every page to top on initial load
    if (isPlatformBrowser(this.platformId)) {
      this.router.events.subscribe((event: NavigationEnd) => {
        window.scroll(0, 0);
      });
    }

    // affix subnavbar when scrolling
    let scrollpos = window.scrollY
    const header:any = document.querySelector(".sub-header");
    const header_height = header.offsetHeight
  
    const add_class_on_scroll = () => header.classList.add("fixed_subHeader")
    const remove_class_on_scroll = () => header.classList.remove("fixed_subHeader")
  
    window.addEventListener('scroll', function() { 
      scrollpos = window.scrollY;
  
      if (scrollpos >= header_height) { add_class_on_scroll() }
      else { remove_class_on_scroll() }

    })

    this.checkScroll();
  }
  ngAfterViewInit(){
    // detect footer height 
    setTimeout(() => {
      let matchMedia = window.matchMedia('(max-width: 991px)');
      console.log(matchMedia);
      if(matchMedia.matches == true){
        this.mainMarginBottom = 0;
      }else{
        this.mainMarginBottom = this.footer.nativeElement.clientHeight;
      }

    }, 100);

  }
  clicBtn(){
    this.initialload = true;
  }
  checkScroll() {
    const scrollPosition = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
    if (scrollPosition >= this.topPosToStartShowing) {
      this.isShow = true;
    } else {
      this.isShow = false;
    }
  }
  
  scrollToTop() {
      window.scroll({ 
        top: 0, 
        left: 0, 
        behavior: 'smooth' 
      });
    }
}
