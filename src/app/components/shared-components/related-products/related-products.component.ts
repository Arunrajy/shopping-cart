import { Component, OnInit, ElementRef, ViewEncapsulation} from '@angular/core';
import { Router } from "@angular/router";
import { ActivatedRoute } from '@angular/router';

declare var tns: any;

@Component({
  selector: 'app-related-products',
  templateUrl: './related-products.component.html',
  styleUrls: ['./related-products.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class RelatedProductsComponent implements OnInit {

  constructor(private router: Router,private activeroute: ActivatedRoute,private elRef : ElementRef) {

   }
  ngAfterViewInit() {
 
  }
  ngOnInit() {
    tns({
      container: '.related-product-slider',
      items: 4,
      slideBy: 1,
      mouseDrag: true,
      autoplay: true,
      nav: false,
      navPosition: "bottom",
      controls: true,
      gutter: 10,
      edgePadding:5,
      autoplayButtonOutput: false,
      controlsText: ["<div class='prev'><i class='fas fa-chevron-left'></i></div>", "<div class='next'><i class='fas fa-chevron-right'></i></div>"],
      responsive: {
        320: {
          items: 1,
          nav: true,
          controls: false,
          slideBy: 1,
        },
        480: {
          items: 2,
          nav: true,
          controls: false,
          slideBy: 1,
        },
        767: {
          items: 3,
          nav: true,
          controls: false,
          slideBy: 1,
        },
        900: {
          items: 4,
          nav: false,
          controls: true,
          slideBy: 1,
        },
      },
    });
  }

}
