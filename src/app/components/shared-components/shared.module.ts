import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RelatedProductsComponent } from "./related-products/related-products.component";
@NgModule({
    imports: [
        CommonModule,
    ],
    declarations: [
        RelatedProductsComponent
    ],
    exports: [
        RelatedProductsComponent
    ]
})
export class SharedModule {

}
