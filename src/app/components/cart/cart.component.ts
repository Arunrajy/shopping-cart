import { Component, OnInit, ElementRef} from '@angular/core';
import { Router } from "@angular/router";
import { ActivatedRoute } from '@angular/router';
declare var mixitup: any;
declare var tns: any;
@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})

export class CartComponent implements OnInit {

  constructor(private router: Router,private activeroute: ActivatedRoute,private elRef : ElementRef) {

   }
  ngAfterViewInit() {
 
  }
  ngOnInit() {
    let mixitupGrid = this.elRef.nativeElement.querySelector(".zerogrid");
    mixitup(mixitupGrid);
    tns({
      container: '.brand_slider',
      items: 6,
      slideBy: 1,
      mouseDrag: true,
      autoplay: true,
      nav: false,
      controls: false,
      autoplayButtonOutput: false
    })
  }

}
