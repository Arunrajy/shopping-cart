import { Component, OnInit, ElementRef, ViewEncapsulation} from '@angular/core';
import { Router } from "@angular/router";
import { ActivatedRoute } from '@angular/router';
declare var tns: any;
declare var lightGallery: any;
@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class ProductDetailComponent implements OnInit {

  constructor(private router: Router,private activeroute: ActivatedRoute,private elRef : ElementRef) {

   }
  ngAfterViewInit() {
 
  }
  ngOnInit() {

    tns({
      container: '.product-slider',
      items: 1,
      slideBy: 1,
      mouseDrag: true,
      autoplay: true,
      nav: true,
      navPosition: "bottom",
      controls: false,
      autoplayButtonOutput: false
    });
    lightGallery(document.getElementById('lightgallery'),{
      download: false,
      counter: false
    });
  }
  increaseValue(number) {
    console.log(number)
    var value = parseInt(number.value, 10);
    value = isNaN(value) ? 0 : value;
    value++;
    number.value = value;
  }
  
  decreaseValue(number) {
    var value = parseInt(number.value, 10);
    value = isNaN(value) ? 0 : value;
    value < 1 ? value = 1 : '';
    value--;
    number.value = value;
  }
}
