import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductListingSidebarComponent } from './product-listing-sidebar.component';

describe('ProductListingSidebarComponent', () => {
  let component: ProductListingSidebarComponent;
  let fixture: ComponentFixture<ProductListingSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductListingSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductListingSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
