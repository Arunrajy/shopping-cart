import { Component, OnInit, ElementRef, ViewEncapsulation} from '@angular/core';
import { Router } from "@angular/router";
import { ActivatedRoute } from '@angular/router';
declare var mixitup: any;
declare var tns: any;
@Component({
  selector: 'app-product-listing-sidebar',
  templateUrl: './product-listing-sidebar.component.html',
  styleUrls: ['./product-listing-sidebar.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class ProductListingSidebarComponent implements OnInit {
  public isCollapsed = true;
  constructor(private router: Router,private activeroute: ActivatedRoute,private elRef : ElementRef) {

   }
  ngAfterViewInit() {
 
  }
  ngOnInit() {
        // affix subnavbar when scrolling
        let scrollpos = window.scrollY
        const header:any = document.querySelector(".toolbox_section");
        const header_height = header.offsetHeight
      
        const add_class_on_scroll = () => header.classList.add("fixed_subHeader")
        const remove_class_on_scroll = () => header.classList.remove("fixed_subHeader")
      
        window.addEventListener('scroll', function() { 
          scrollpos = window.scrollY;
      
          if (scrollpos >= header_height) { add_class_on_scroll() }
          else { remove_class_on_scroll() }
    
        })
  }

}
