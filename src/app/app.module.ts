import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HomeComponent } from "./components/home/home.component";
import { ProductListingComponent } from "./components/product-listing/product-listing.component";
import { ProductListingSidebarComponent } from "./components/product-listing-sidebar/product-listing-sidebar.component";
import { ProductDetailComponent } from "./components/product-detail/product-detail.component";
import { CartComponent } from "./components/cart/cart.component";
import { CheckoutComponent } from "./components/checkout/checkout.component";
import { HelpComponent } from "./components/help/help.component";
import { TrackOrderComponent } from "./components/track-order/track-order.component";
import { ConfirmComponent } from "./components/confirm/confirm.component";
import {ContactComponent } from "./components/contact/contact.component";
import { SharedModule } from "./components/shared-components/shared.module";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ProductListingComponent,
    ProductListingSidebarComponent,
    ProductDetailComponent,
    CartComponent,
    CheckoutComponent,
    HelpComponent,
    TrackOrderComponent,
    ConfirmComponent,
    ContactComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
